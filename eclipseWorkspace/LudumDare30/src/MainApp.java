import java.util.ArrayList;

import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import toxi.geom.mesh.Mesh3D;
import toxi.geom.mesh.Terrain;
import toxi.processing.ToxiclibsSupport;

// This is from Processing2.2.1

@SuppressWarnings("serial")
public class MainApp extends PApplet {

	// import peasy.PeasyCam;

	int swidth = 1200;
	int sheight = 800;

	final static String GAMEMODE_MENU = "GAMEMODE_MENU";
	final static String GAMEMODE_PLAY = "GAMEMODE_PLAY";
	String GAMEMODE = GAMEMODE_MENU;

	//
	PeasyCam cam;

	// OBJECTS Should be considered in real world coordinates. Objects Should Not move by default. Currently, it is the environment that moves - though ideally it should be the camera that moves.

	//
	// let's assume 100px = 1m - i.e. 1px = 1cm

	// int areaDim = 2000; // ground plane area
	PVector areaDims = new PVector(5000, 0, 3000);
	PVector cameraPos = new PVector(0, 200, 0);

	//
	ArrayList<Tree> trees;
	//
	int framesSinceLastObstacleAddition = 0;
	ArrayList<Obstacle> obstacles;
	//
	ArrayList<Boost> boosts;

	ArrayList<PVector> buildingPositions;

	//
	Hero hero = new Hero();
	float heroZposition = -areaDims.z * 0.2f;
	int finishLinePositionX = 700 * 100; // x metres

	float heroOffsetFromLeftBoundOfArea = areaDims.x * 0.44f;

	//
	boolean wireframeModeOn = false;
	// String[] LIGHTMODES = { "NONE", "DEFAULT", "CUSTOM", "DEFAULT_AND_CUSTOM" };
	int LIGHTMODE_NONE = 0;
	int LIGHTMODE_DEFAULT = 1;
	int LIGHTMODE_CUSTOM = 2;
	int LIGHTMODE_DEFAULT_AND_CUSTOM = 3;

	int LIGHTMODES_NUMBER = 4;

	int LIGHTMODE = LIGHTMODE_CUSTOM;

	//
	PImage skyImage;
	//
	Terrain terrain;
	Mesh3D terrainMesh;
	ToxiclibsSupport gfx;

	float overallProgress = 0; // in-level progress

	boolean keyIsHeld = false; // to prevent held-jumping

	public void setup() {
		size(swidth, sheight, OPENGL);
		colorMode(HSB, 100);

		// frameRate(10000);

		// PERSPECTIVE
		float cameraZ = (height / 2.0f) / tan(PI * 60.0f / 360.0f);
		// Default // perspective(PI / 3.0f, width * 1f / height, cameraZ / 10.0f, cameraZ * 10.0f);
		// Set clipping plane further out
		perspective(PI / 3.0f, width * 1f / height, cameraZ / 10.0f, cameraZ * 1000.0f);

		// Initialize camera
		cam = new PeasyCam(this, 1800);

		//
		boosts = new ArrayList<Boost>();
		//
		obstacles = new ArrayList<Obstacle>();
		// Initialize some bg objects
		trees = new ArrayList<Tree>();

		//
		buildingPositions = new ArrayList<PVector>();
		for (int i = 0; i < 100; i++) {
			buildingPositions.add(new PVector(random(1), random(1)));
		}

		//
		skyImage = loadImage("QuickPhotoshopClouds2.jpg");

		// TOXIC � ATTACH DRAWING UTILS
		gfx = new ToxiclibsSupport(this);

	}

	public void draw() {
		if (GAMEMODE.equals(GAMEMODE_MENU)) {
			this.draw_MenuMode();
		} else if (GAMEMODE.equals(GAMEMODE_PLAY)) {
			this.draw_PlayMode();
		}
	}

	public void draw_MenuMode() {
		background(35, 50, 80);
		// Draw some text
		cam.beginHUD();
		fill(0, 0, 100);
		stroke(0, 0, 100);
		textSize(24);
		String s = "";
		s += "Times Stumbled: " + hero.stat_timesHit;
		s += "\n";
		s += "Click anywhere to play.";
		s += "\n";
		s += "Instructions: \n";
		s += "\t Press Space to Jump. \n";
		s += "\t Avoid the red obstacles. \n";
		s += "\t Collect yellow Boosts to go faster. \n";
		text(s, 50, 50);
		cam.endHUD();

		// Register a click as an indicator to start playing
		if (mousePressed == true) {
			this.segueToGameMode(GAMEMODE_PLAY);
		}
	}

	public void draw_PlayMode() {
		background(45, 80, 20);

		overallProgress = map(mouseX, 0, swidth, 0, 1);

		float levelProgress = hero.xpos * 1f / finishLinePositionX;

		float transitionProgress = map(levelProgress, 0.4f, 0.6f, 0f, 1f);

		overallProgress = transitionProgress; // map(heroZposition, 0, finishLinePositionX, 0, 1);
		// overallProgress = map(overallProgress, 0.4f, 0.6f, 0, 1);
		overallProgress = Math.max(overallProgress, 0f);
		overallProgress = Math.min(overallProgress, 1f);

		// overallProgress = 1;

		cam.beginHUD();
		// DRAW SKY
		image(skyImage, 0, 0);
		cam.endHUD();

		if ((LIGHTMODE == LIGHTMODE_DEFAULT) || (LIGHTMODE == LIGHTMODE_DEFAULT_AND_CUSTOM)) {
			lights();
		}
		if ((LIGHTMODE == LIGHTMODE_CUSTOM) || (LIGHTMODE == LIGHTMODE_DEFAULT_AND_CUSTOM)) {
			// DEFAULT LIGHTS
			lightFalloff(1, 0, 0); // like fill - for lights
			lightSpecular(0, 0, 0); // like fill - for lights
			ambientLight(0, 0, 50);
			// directionalLight(0, 0, 75, 0, 0, -1);
			// directionalLight(0, 0, 75, 0, -1, 0);
			directionalLight(0, 0, 75, 0, -1, -1);

			// POINT LIGHT 1 - Red Light - Good addition for Sandy Light - also beneficial as added lighting for forest
			PVector pointLightHsb = new PVector(6, 100, 100);
			pointLight(pointLightHsb.x, pointLightHsb.y, pointLightHsb.z, 0, 0, 0);

			// POINT LIGHT 2
			// pointLight(0, 0, 100, 0, -2000, -2000);
		}

		this.archived_drawAndUpdateTerrainAndFog();

		// DRAW GROUND
		cam.beginHUD();
		noStroke();
		pushStyle();
		// Transitional color
		PVector groundColor_sandy = new PVector(15, 35, 90);
		PVector groundColor_forest = new PVector(131 * 100f / 360, 53, 30);
		float h = map(overallProgress, 0, 1, groundColor_sandy.x, groundColor_forest.x);
		float s = map(overallProgress, 0, 1, groundColor_sandy.y, groundColor_forest.y);
		float b = map(overallProgress, 0, 1, groundColor_sandy.z, groundColor_forest.z);
		fill(h, s, b);
		emissive(h, s, b - 20);
		pushMatrix();
		translate(0, 20);
		rect(0, sheight / 2, swidth, sheight / 2);
		popStyle();
		popMatrix();
		cam.endHUD();

		// DRAW GROUND PLANE
		// this.archived_drawGroundPlane();

		//
		this.archived_drawAndUpdateObstacles();
		this.archived_drawAndUpdateBoosts();

		// /////////////////////////////////////////////////////////

		// UPDATE CAMERA POSITION
		cameraPos.x = hero.xpos - heroOffsetFromLeftBoundOfArea;
		// Note - cameraPosX actually represents the left-most x-axis on screen (rather than the centre)

		// /////////////////////////////////////////////////////////
		this.archived_drawAndUpdateBuildings();

		// /////////////////////////////////////////////
		// HERO
		hero.update();
		// DRAW HERO
		pushMatrix();
		translate(-cameraPos.x, cameraPos.y, 0); // Camera Position
		translate(-areaDims.x / 2, 0, areaDims.z / 2); // Grid Position
		hero.draw_objectAtLocation();
		popMatrix();

		// /////////////////////////////////////////////////////////
		// HERO PROGRESS
		float progress = hero.xpos * 1f / finishLinePositionX;
		progress = Math.min(progress, 1);
		if (progress == 1) {
			// set mode from running to finished
			hero.MODE = hero.MODE_FINISHED;
		}
		// Go back to Game screen when hero is ready too
		if (hero.MODE.equals(hero.MODE_FINISHED)) {

			if (hero.xvel > 1) {
				hero.xvel = (float) Math.pow(hero.xvel, 0.5f); // give it a bit of a push to finish
			}

			if (hero.xvel == 0) {
				this.segueToGameMode(GAMEMODE_MENU);
			}
		}

		// DRAW TREES
		this.archived_drawAndUpdateTrees();

		// /////////////////////////////////////////////////////
		this.archived_drawText();

	}

	public void archived_drawAndUpdateTerrainAndFog() {
		// PARAMETRES
		// SANDY SETTINGS
		// boolean shouldDrawFog = false;
		// boolean shouldBeSandy = true;
		// boolean shouldBeSmooth = true;
		// PVector terrainFillColor = new PVector(100 * 51f / 360, 50, 95);
		// float terrainFillColorAlpha = 100;
		// int terrainMaxHeight = 3000 * 1;
		// float terrainNoiseValPower = 2; // smoothing
		// float fog_maxAlpha = 0;

		// FOREST SETTING
		boolean shouldDrawFog = true;
		boolean shouldBeSandy = false;
		boolean shouldBeSmooth = true;
		PVector terrainFillColor = new PVector(100 * 125f / 360, 75, 33);
		float terrainFillColorAlpha = 100;
		int terrainMaxHeight = 3000 * 36;
		float terrainNoiseValPower = 8; // smoothing
		PVector fogEmissiveColor = new PVector(164 * 100f / 360, 76, 72);
		float fog_maxAlpha = 100;

		// /////////////////////////////////////
		// Let's try a linear mapping of terrainFillColor
		PVector terrainFillColor_Sandy = new PVector(100 * 51f / 360, 50, 95);
		PVector terrainFillColor_Forest = new PVector(100 * 125f / 360, 75, 33);

		float h = map(overallProgress, 0, 1, terrainFillColor_Sandy.x, terrainFillColor_Forest.x);
		float s = map(overallProgress, 0, 1, terrainFillColor_Sandy.y, terrainFillColor_Forest.y);
		float b = map(overallProgress, 0, 1, terrainFillColor_Sandy.z, terrainFillColor_Forest.z);

		terrainFillColor = new PVector(h, s, b);

		// Of MaxHeight
		int terrainMaxHeight_Sandy = 3000 * 1;
		int terrainMaxHeight_Forest = 3000 * 36;
		terrainMaxHeight = (int) map(overallProgress, 0, 1, terrainMaxHeight_Sandy, terrainMaxHeight_Forest);

		// // Should more likely be two separate terrains
		float terrainNoiseValPower_Sandy = 2;
		float terrainNoiseValPower_Forest = 8;
		terrainNoiseValPower = map(overallProgress, 0, 1, terrainNoiseValPower_Sandy, terrainNoiseValPower_Forest);
		//

		// Of Fog
		float fog_maxAlpha_Sandy = 0;
		float fog_maxAlpha_Forest = 100;
		fog_maxAlpha = map(overallProgress, 0, 1, fog_maxAlpha_Sandy, fog_maxAlpha_Forest);

		if (overallProgress < 0.5f) {
			shouldBeSandy = true;
		} else {
			shouldBeSandy = false;
		}

		// Fog Fade

		// ///////////////////////////////

		// Set Values
		float zBoundNear = -4000; // 0;// -areaDim / 2;
		// float zBoundFar = zBoundNear - 9000;
		float maxWidthNear = 2000 * 4.7f * 3; // 2000 * 2;//
		// float maxWidthFar = maxWidthNear;// maxWidthNear * 2.75f;
		// float maxHeightNear = 600;

		// Update terrain
		// Terrain

		int terrainCellsWidth = 80;
		int terrainCellsDepth = 80;

		float terrainRealWidth = maxWidthNear;

		float terrainNoiseScale = 0.08f;

		float terrainScale = terrainRealWidth * 1f / terrainCellsWidth;

		float terrainRealDepth = terrainCellsDepth * terrainScale;

		// zBoundFar = zBoundNear + terrainRealDepth;

		terrain = new Terrain(terrainCellsWidth, terrainCellsDepth, terrainScale);

		float cellsTraversedInOneAreaDim = terrainCellsWidth * areaDims.x * 1f / terrainRealWidth; // <- Would be ideal if this didn't rely on areaDim for calculation
		float curTerrainNoiseXoffset = map(hero.xpos, 0, areaDims.x, 0, cellsTraversedInOneAreaDim);

		float progressThroughOneCell = curTerrainNoiseXoffset % 1f; // * Used to fix point position and remove 'sand-dune' effect

		float[] elevations = new float[terrainCellsWidth * terrainCellsDepth];
		for (int z = 0, i = 0; z < terrainCellsDepth; z++) {
			for (int x = 0; x < terrainCellsWidth; x++) {
				float t = curTerrainNoiseXoffset;
				if (!shouldBeSandy) {
					t = curTerrainNoiseXoffset - progressThroughOneCell; // *
				}
				float noiseVal = noise((x + t) * terrainNoiseScale, z * terrainNoiseScale);

				// scale noiseVal
				noiseVal = (float) Math.pow(noiseVal, terrainNoiseValPower);

				// make the closer terrain slightly shorter
				float curTerrainMaxHeight = terrainMaxHeight;
				curTerrainMaxHeight = map(z, terrainCellsDepth, 0, terrainMaxHeight * 0.75f, terrainMaxHeight);

				// make very close terrain taper down
				float noiseValManip = map(z, terrainCellsDepth, terrainCellsDepth - 4, 0, 1); // 4 - maxDistance to manip
				noiseValManip = Math.min(1, noiseValManip);
				curTerrainMaxHeight *= noiseValManip;

				// elevations[i++] = -noiseVal * terrainMaxHeight;
				elevations[i++] = -noiseVal * curTerrainMaxHeight;
			}
		}
		terrain.setElevation(elevations);
		// create mesh
		terrainMesh = terrain.toMesh(0);

		// Draw Terrain Mesh
		pushMatrix();
		translate(0, cameraPos.y, 0);

		// Offset terrain
		translate(0, 0, zBoundNear);

		// Offset - ProgressThroughCell // *
		float xOffset = progressThroughOneCell * areaDims.x / cellsTraversedInOneAreaDim; // <- Would be ideal if this didn't rely on areaDim for calculation
		if (!shouldBeSandy) {
			translate(-xOffset, 0, 0);
		}

		// Pivot terrain by Front-Middle
		translate(0, 0, -terrainRealDepth / 2);

		pushStyle();
		noStroke();
		fill(terrainFillColor.x, terrainFillColor.y, terrainFillColor.z, terrainFillColorAlpha);
		// fill(60, 44, 74);
		// fill(60, 44, 74, 20);
		// emissive(59, 55, 5);
		// gfx.mesh(terrainMesh, false);
		gfx.mesh(terrainMesh, shouldBeSmooth);
		// filter(INVERT);
		popStyle();
		popMatrix();

		// ////////////////////////////////////////////
		// Fog Attempt
		// NOTE - the order at which 3D Alpha objects are drawn effect if the Alpha Effect stacks

		// Fog as box

		if (shouldDrawFog) {
			pushStyle();
			float boxFrontExtrusionAmount = 3000 - zBoundNear;
			float distanceInletDepth = terrainRealDepth * 0.5f;
			float distanceInletSides = terrainRealWidth * 0.2f;

			pushMatrix();
			if (wireframeModeOn) {
				stroke(0, 0, 100);
				noFill();
			} else {
				noStroke();
			}
			// emissive(65, 50, 20);
			// emissive(60, 10, 95);
			// emissive(60, 5, 50);
			// emissive(45, 80, 20);
			emissive(fogEmissiveColor.x, fogEmissiveColor.y, fogEmissiveColor.z);

			translate(0, 0, boxFrontExtrusionAmount);
			translate(0, 0, zBoundNear);

			// Note - the number of filled Rectangles - at 10 - drops fps from ~90-65 , ∂ ~25fps
			for (float i = 0; i < 1.0; i += 0.1f) {
				pushMatrix();
				float boxw = map(i, 0, 1, terrainRealWidth, terrainRealWidth - distanceInletSides);
				boxw *= 0.9f; // <- bit of a shortcut buffer to handle the non-sandy offset of noisefield
				float boxd = map(i, 0, 1, terrainRealDepth, terrainRealDepth - distanceInletDepth);
				boxd += boxFrontExtrusionAmount;

				double remappedX = Math.pow(i, 0.25f);
				float alpha = map((float) remappedX, 0, 1, fog_maxAlpha, 0);
				if (!wireframeModeOn) {
					fill(0, 0, 0, alpha);
				}

				translate(0, 0, -boxd / 2); // pivot point
				box(boxw, terrainMaxHeight, boxd);
				popMatrix();
			}

			popStyle();
			popMatrix();
		}

		// filter(INVERT); // InvertFilter - uses up quite a few fps - drops down to ~35 from 60, ∂ ~25fps
	}

	public void archived_drawAndUpdateBuildings() {
		// /////////////////////////////////////////////////////////
		// DRAW SOME BUILDINGS

		// Set Values
		float zBoundNear = -4000;
		float zBoundFar = zBoundNear - 9000;
		float maxWidthNear = 2000 * 4.7f;
		float maxWidthFar = maxWidthNear * 2.75f;
		float maxHeightNear = 600;
		float maxHeightFar = 3600;

		// Draw Bounding Volume
		if (wireframeModeOn) {
			pushMatrix();
			translate(0, cameraPos.y, 0);
			strokeWeight(1);
			stroke(0, 0, 80);
			noFill();
			beginShape(QUAD_STRIP);
			vertex(-maxWidthNear / 2, 0, zBoundNear);
			vertex(maxWidthNear / 2, 0, zBoundNear);
			vertex(-maxWidthNear / 2, -maxHeightNear, zBoundNear);
			vertex(maxWidthNear / 2, -maxHeightNear, zBoundNear);
			vertex(-maxWidthFar / 2, -maxHeightFar, zBoundFar);
			vertex(maxWidthFar / 2, -maxHeightFar, zBoundFar);
			vertex(-maxWidthFar / 2, 0, zBoundFar);
			vertex(maxWidthFar / 2, 0, zBoundFar);
			endShape();
			popMatrix();
		}

		pushMatrix();
		translate(-cameraPos.x, cameraPos.y, 0); // Camera Position
		// translate(-areaDim / 2, 0, areaDim / 2); // Grid Position
		// translate(cameraPos.x, 0, 0); // Temporarily restrain movement

		// Building Vars
		int width = 600;
		int depth = 600;
		//
		for (int i = 0; i < buildingPositions.size(); i++) {
			pushMatrix();
			// Set position of building
			PVector p = buildingPositions.get(i);
			float z = map(p.y, 0, 1, zBoundNear, zBoundFar);
			float maxWidthCur = map(p.y, 0, 1, maxWidthNear, maxWidthFar);
			float x = map(p.x, 0, 1, -maxWidthCur / 2, maxWidthCur / 2);
			float height = map(p.y, 0, 1, maxHeightNear, maxHeightFar);
			translate(x, 0, z);

			// Set Pivot Point
			translate(0, -height / 2, -depth / 2);

			float curLeftXbound = map(p.y, 0, 1, -maxWidthNear / 2, -maxWidthFar / 2);

			// Remove building if out of view
			if ((x - cameraPos.x + width * 1.8f) < curLeftXbound) { // added a bit of buffer
				buildingPositions.remove(i);
				i--;
				// strokeWeight(5);
				// stroke(0, 100, 100);
			} else {
				// strokeWeight(1);
				// stroke(0, 0, 100);
			}

			// Draw

			if (wireframeModeOn) {
				noFill();
				stroke(0, 0, 100);
			} else {
				noStroke();
				strokeWeight(1);
				stroke(0, 0, 100, 20);
				fill(25, 80, 60);
			}
			box(width, height, depth);
			popMatrix();

		}
		popMatrix();

		// Add new buildings
		if (random(1) < 0.05f) {
			// Add as pctg
			// add at the right-most boundary

			float y = random(1);
			float maxWidthCur = map(y, 0, 1, maxWidthNear, maxWidthFar);
			float x = cameraPos.x / maxWidthCur + 1;
			x += 0.1f; // add a tiny bit so it doesn't suddenly appear
			buildingPositions.add(new PVector(x, y));
		}
	}

	public void archived_drawAndUpdateBoosts() {
		// /////////////////////////////////////////////
		// BOOSTS - Add new Boosts
		if (random(1) < 0.015f) {
			float x = cameraPos.x + areaDims.x; // ground line
			float y = -300; // ~
			float z = heroZposition;
			PVector pos = new PVector(x, y, z);
			boosts.add(new Boost(pos));

		}
		// Draw & Update Boosts
		pushMatrix();
		// Camera Position
		translate(-cameraPos.x, cameraPos.y, cameraPos.z);
		// Grid Origin Position
		translate(-areaDims.x / 2, 0, areaDims.z / 2);
		pushStyle();
		for (int i = 0; i < boosts.size(); i++) {
			boolean shouldDeleteBoost = false;

			// Get Boost
			Boost boost = boosts.get(i);

			// Update Boost
			boost.update();

			// Check if hero touches Boost
			if (boost.hitTest(hero.centrePointPosition, hero.sphereRadius * 1.75f)) {
				// Boost Hero
				hero.shouldBeBoosted = true;
				// Delete boost
				shouldDeleteBoost = true;
				boost.radius = 100;
			}

			// Draw Boost
			boost.drawObjectAtLocation();

			// Mark items out of view for deletion
			if (boost.curLocation.x < cameraPos.x) {
				shouldDeleteBoost = true;
			}

			// Delete Boosts
			if (shouldDeleteBoost) {
				boosts.remove(i);
				i--;
			}

		}
		popStyle();
		popMatrix();
	}

	public void archived_drawAndUpdateObstacles() {
		// /////////////////////////////////////////////
		// Obstacles - Update
		if (random(1) < 0.01f) {
			// Only allow obstacle addition after a minimum distance of 70 frames (can convert this into absolute coorinates later)
			if (framesSinceLastObstacleAddition > 70) {
				float x = cameraPos.x + areaDims.x;
				float y = heroZposition;

				obstacles.add(new Obstacle(new PVector(x, 0, y)));

				// adding obstacles as absolute positions

				framesSinceLastObstacleAddition = 0;
			}
		} else {
			framesSinceLastObstacleAddition++;
		}

		// Draw Obstacles
		pushMatrix();
		translate(-cameraPos.x, cameraPos.y, cameraPos.z);
		translate(-areaDims.x / 2, 0, areaDims.z / 2);
		pushStyle();
		for (int i = 0; i < obstacles.size(); i++) {
			// Get Obstacle
			Obstacle o = obstacles.get(i);

			// Draw Obstacle
			o.drawObjectAtLocation();

			// Knock back hero if it hits obstacle
			if (o.hitTest(hero.centrePointPosition, hero.sphereRadius)) {
				// If hero isn't immune, it is effected
				if (!hero.isImmune) {
					// Knock back hero
					hero.xpos -= 60;
					hero.xvel = hero.xvel_stable;

					// Temporarily turn on immunity
					hero.isImmune = true;
					// Reset Immunity Timer
					hero.immunityTime_cur = 0;

					// update stat
					hero.stat_timesHit++;
				}
			}

			// Delete old obstacles
			if (o.location.x < cameraPos.x) {
				obstacles.remove(i);
				i--;
			}

		}
		popStyle();
		popMatrix();
	}

	public void archived_drawAndUpdateTrees() {
		if (random(1) < 0.1f) {
			float x = cameraPos.x + areaDims.x;
			float z = -random(areaDims.z);

			trees.add(new Tree(new PVector(x, 0, z)));
			// adding obstacles as absolute positions
		}

		// Draw Trees
		// Draw Obstacles
		pushMatrix();
		translate(-cameraPos.x, cameraPos.y, cameraPos.z);
		translate(-areaDims.x / 2, 0, areaDims.z / 2);
		pushStyle();
		for (int i = 0; i < trees.size(); i++) {
			// Get Obstacle
			Tree tree = trees.get(i);

			// Draw Tree
			tree.drawObjectAtLocation();

			// Delete old trees
			if (tree.location.x < cameraPos.x) {
				trees.remove(i);
				i--;
			}

		}
		popStyle();
		popMatrix();
	}

	public void keyPressed() {
		// Press 'g' to switch gameModes
		if (GAMEMODE.equals(GAMEMODE_MENU)) {
			if (key == 'g') {
				this.segueToGameMode(GAMEMODE_PLAY);
			}
		} else if (GAMEMODE.equals(GAMEMODE_PLAY)) {
			if (key == 'g') {
				this.segueToGameMode(GAMEMODE_MENU);
			}
			if ((key == ' ')) {
				if (!keyIsHeld) {
					hero.requestToJump();
					keyIsHeld = true;
				}
			}
			if (key == 's') {
				hero.xvel_stable_duringRunningMode = 0f;
				hero.xvel_stable = 0.0f;
				hero.xvel = 0f;
			}
		}
		// Camera reset
		if ((key == 'c') || (key == 'r')) {
			cam.reset(200);
		}
		// Lights toggle
		if (key == 'l') {
			LIGHTMODE++;
			LIGHTMODE %= LIGHTMODES_NUMBER;
		}
		// Wireframe toggle
		if (key == 'w') {
			wireframeModeOn = !wireframeModeOn;
		}
	}

	public void keyReleased() {
		keyIsHeld = false;
	}

	public void segueToGameMode(String targetMode) {
		if (targetMode.equals(GAMEMODE_PLAY)) {
			if (GAMEMODE.equals(GAMEMODE_MENU)) {
				// Reset Play Variables
				hero.resetVariablesForStartOfLevel();

				// Set GameMode to Play
				GAMEMODE = GAMEMODE_PLAY;
			} else {
				System.err.println("Already in play mode.");
			}

		} else if (targetMode.equals(GAMEMODE_MENU)) {
			GAMEMODE = GAMEMODE_MENU;
		}
	}

	// CLASSES
	class Hero {
		int curJumpCount = 0;
		int maxJumpCount = 2;

		// boolean isJumping = false;
		float ypos = 0;
		float yvel = 0;
		float yacc = 0;
		float yacc_fromGravity = -0.7f;
		float yacc_fromJump = 6f;
		// ^- note - the rate at which these apply are effected by framerate
		//

		boolean shouldBeBoosted = false;
		float xpos = 0;
		float xvel = 0;
		float xacc = 0;
		float xvel_stable;

		// these should be made relative to frameRate
		float xvel_stable_duringRunningMode = 14.0f; // 7 at 60fps, 14 at 30fps
		float xacc_fromBoost = 5.0f * 3;
		float x_decelerationToStableVelocity = 0.05f * 2f;

		final float sphereRadius = 60;

		boolean isImmune = false;
		int immunityTime_cur = 0;
		final int immunityTime_max = 60;

		PVector centrePointPosition = new PVector(xpos, ypos, heroZposition);

		String MODE_FINISHED = "MODE_FINISHED";
		String MODE_RUNNING = "MODE_RUNNING";
		String MODE = MODE_RUNNING;

		int stat_timesHit;

		Hero() {
			this.resetVariablesForStartOfLevel();
		}

		void resetVariablesForStartOfLevel() {
			// isJumping = false;
			curJumpCount = 0;
			ypos = 0;
			yvel = 0;
			yacc = 0;
			shouldBeBoosted = false;
			xpos = 0;
			xvel = 0;
			xacc = 0;
			isImmune = false;
			immunityTime_cur = 0;

			centrePointPosition = new PVector(xpos, ypos, heroZposition);

			this.MODE = MODE_RUNNING;

			stat_timesHit = 0;
		}

		void requestToJump() {
			// If on the ground then allow jump
			// if (ypos == 0) {
			// Jump (a temporary acceleration basically
			if (curJumpCount < maxJumpCount) {
				curJumpCount++;
				yacc = 0;
				yvel = 0;
				yacc += yacc_fromJump;
			}
			// }
		}

		void update() {
			// Tweak Variables
			// heroYdecelerationFromGravity = -0.11f;
			// heroYaccelerationFromJump = -heroYdecelerationFromGravity * 16.0f;
			// heroXstableVelocity = 7.0f;
			// heroXaccelerationFromBoost = 5.0f;
			// heroXdecelerationToStableVelocity = 0.05f;

			// /////////////////////
			if (this.MODE.equals(MODE_RUNNING)) {
				xvel_stable = xvel_stable_duringRunningMode;
			} else if (this.MODE.equals(MODE_FINISHED)) {
				xvel_stable = 0;
			}

			this.update_position();

			this.update_immunity();
		}

		void update_position() {
			// update hero Y
			// ~ For smoother movement - perhaps jump should be more parabolic-maths orientated

			// xvel = mouseX*0.1f; // ~

			// update y
			yacc += yacc_fromGravity;
			yvel += yacc;
			ypos += yvel;

			// if y is grounded or below, set all values to 0
			if (ypos <= 0) {
				curJumpCount = 0;
				yacc = 0;
				yvel = 0;
				ypos = 0;
			}

			// update hero X
			if (shouldBeBoosted) {
				xacc += xacc_fromBoost;
				shouldBeBoosted = false;

				// also lets give an extra jump to the player
				curJumpCount--;

			} else {
				xacc = 0;
			}
			if (xvel >= xvel_stable) {
				xvel -= x_decelerationToStableVelocity;
			} else {
				xvel = xvel_stable;
			}
			if (xvel < 0) {
				xvel = 0;
			}

			xvel += xacc;
			xpos += xvel;

			// update heroCentrePosition
			centrePointPosition.x = xpos;
			centrePointPosition.y = -ypos - sphereRadius / 2;
			centrePointPosition.z = heroZposition;
		}

		void update_immunity() {
			// Update Hero Immunity
			if (isImmune) {
				immunityTime_cur++;
				if (immunityTime_cur >= immunityTime_max) {
					immunityTime_cur = 0;
					isImmune = false;
				}
			}
		}

		void draw_objectOnly() {
			translate(0, -sphereRadius, 0);
			rotateZ(xpos * 0.01f);
			noFill();
			fill(200 / 3.6f, 100, 50);
			stroke(0, 0, 100);
			sphereDetail(12);
			strokeWeight(1);
			if (isImmune) {
				strokeWeight(frameRate * 0.1f % 3);
			}
			sphere(sphereRadius); // avg human height ~180cm
		}

		void draw_objectAtLocation() {

			// Hero's Position
			translate(xpos, -ypos, heroZposition);

			// Draw Hero Sphere
			this.draw_objectOnly();
		}

	}

	class Boost {
		PVector coreLocation;
		PVector curLocation;
		float radius = 50;
		float randomBobbingTimeOffset = 0;

		Boost(PVector coreLocation) {
			this.coreLocation = coreLocation;
			this.curLocation = this.coreLocation;

			randomBobbingTimeOffset = random(100);
		}

		void update() {
			// Make the coreLocation bob up and down
			float time = (frameCount + randomBobbingTimeOffset) * 0.05f;
			float yoffset = (float) Math.sin(time) * 6;
			curLocation = new PVector(coreLocation.x, coreLocation.y + yoffset, coreLocation.z);
		}

		void drawObject() {
			strokeWeight(1);
			stroke(15, 80, 80);
			fill(15, 100, 100);
			// sphere(radius);
			sphere(radius * 0.5f);
		}

		void drawObjectAtLocation() {
			pushMatrix();
			translate(curLocation);
			this.drawObject();
			popMatrix();
		}

		boolean hitTest(PVector hitPoint, float hitTestRadius) {
			float maxHitTestRadius = Math.max(this.radius, hitTestRadius);
			if (hitPoint.dist(curLocation) < maxHitTestRadius) {
				return true;
			}
			return false;
		}
	}

	class Tree {
		PVector location;
		private float rotation;

		Tree(PVector location) {
			this.location = location;
			rotation = random(2 * PI);
		}

		void drawObject() {
			// Draw 'Tree'

			int treeHeight = 1000;
			int treeWidthAndDepth = 200;
			int branchesMinHeight = 400;

			boolean shouldDrawBranches = true;

			rotateY(rotation); // ~

			float treeAlpha = 100;
			boolean treeInFrontOfHero = false;
			if (location.z > heroZposition - 200) {
				// treeAlpha = 10;
				treeInFrontOfHero = true;
			}

			// DRAW TRUNK
			pushMatrix();
			translate(0, -treeHeight / 2, 0);
			fill(28 / 3.6f, 68, 39, treeAlpha);
			noStroke();
			if (treeInFrontOfHero) {
				strokeWeight(1);
				stroke(0, 0, 100);
				noFill();
				// noStroke();
			}
			box(treeWidthAndDepth, treeHeight, treeWidthAndDepth);
			popMatrix();

			// DRAW PYRAMID
			if (shouldDrawBranches) {
				noStroke();
				fill(93 / 3.6f, 75, 62, treeAlpha);
				if (treeInFrontOfHero) {
					strokeWeight(1);
					stroke(0, 0, 100);
					noFill();
				}

				int maxPyramids = 4;
				for (int i = 0; i < maxPyramids; i++) {
					// int pWidthDepth = 300;
					int pdim = (int) map(i, 0, maxPyramids, 300, 150);

					int pheight = pdim;
					int pwidth = pdim;
					int pdepth = pdim;
					// ^ - note, the above dimensions aren't labeled correctly since the object has been rotated

					pushMatrix();
					float py = map(i, 0, maxPyramids, -branchesMinHeight, -treeHeight - pheight);
					translate(0, py, 0);

					translate(0, -pheight, 0);
					rotateX(PI / 2);
					beginShape(TRIANGLE_STRIP);
					vertex(-pwidth, -pheight, -pdepth);
					vertex(pwidth, -pheight, -pdepth);
					vertex(0, 0, pdepth);

					vertex(pwidth, -pheight, -pdepth);
					vertex(pwidth, pheight, -pdepth);
					vertex(0, 0, pdepth);

					vertex(pwidth, pheight, -pdepth);
					vertex(-pwidth, pheight, -pdepth);
					vertex(0, 0, pdepth);

					vertex(-pwidth, pheight, -pdepth);
					vertex(-pwidth, -pheight, -pdepth);
					vertex(0, 0, pdepth);
					endShape();

					// DRAW PYRAMID BASE
					if (!treeInFrontOfHero) {
						pushStyle();
						// fill(0, 0, 100, 50);
						fill(93 / 3.6f, 75, 62 - 10, treeAlpha);
						pushMatrix();
						translate(0, 0, -pdim);
						rect(-pdim, -pdim, pdim * 2, pdim * 2);
						popMatrix();
						popStyle();
					}

					popMatrix();

				}
			}

		}

		void drawObjectAtLocation() {
			pushMatrix();
			translate(location);
			this.drawObject();
			popMatrix();
		}

	}

	class Obstacle {
		PVector location;

		float radius = 30;

		Obstacle(PVector location) {
			this.location = location;
		}

		void drawObject() {
			strokeWeight(1);
			stroke(4, 80, 80);
			fill(4, 100, 100);
			sphere(radius);
		}

		void drawObjectAtLocation() {
			pushMatrix();
			translate(location);
			this.drawObject();
			popMatrix();
		}

		boolean hitTest(PVector hitPoint, float hitTestRadius) {
			float maxHitTestRadius = Math.max(this.radius, hitTestRadius);
			if (hitPoint.dist(location) < maxHitTestRadius) {
				return true;
			}
			return false;
		}

	}

	// ADDITIONAL HELPER FUNCTIONS
	void translate(PVector v) {
		translate(v.x, v.y, v.z);
	}

	// ARCHIVED FUNCTIONS

	public void archived_drawText() {
		cam.beginHUD();
		// Draw Framerate
		pushStyle();
		fill(0, 0, 100);
		stroke(0, 0, 100);
		textSize(15);
		text("FrameRate: " + (int) frameRate + " fps", 20, 20);

		// Draw Progress
		float progress = hero.xpos * 1f / finishLinePositionX;
		progress = Math.min(progress, 1);

		fill(0, 0, 90, 20);
		int barWidth = (int) (swidth * 0.75f);
		pushMatrix();
		translate(swidth / 2 - barWidth / 2, 50);
		strokeWeight(1);
		stroke(0, 0, 100, 40);
		// Base Line
		line(0, 0, barWidth, 0);
		// Progress Line
		strokeWeight(2);
		stroke(0, 0, 100, 80);
		line(0, 0, barWidth * progress, 0);
		// Progress Ball
		strokeWeight(4);
		point(barWidth * progress, 0);

		popMatrix();
		popStyle();
		cam.endHUD();
	}

	public void archived_drawGroundPlane() {
		// Draw a ground plane
		pushMatrix();
		translate(0, cameraPos.y, 0);
		// Style
		noFill();
		stroke(0, 0, 100);
		// Variables

		// Draw Centre Point of Ground Plane
		box(3, 3, 3);

		// Draw Ground Plane - representing size of areaDim
		strokeWeight(1);
		box(areaDims.x, 0, areaDims.z);

		// Draw Ground Plane Lines
		pushMatrix();
		translate(-areaDims.x / 2, 0, -areaDims.z / 2);
		rotateX(PI / 2);
		// Draw 'x' gridline - offset by groundProgressX
		for (int x = (int) cameraPos.x; x <= cameraPos.x + areaDims.x; x++) {
			if (x % 100 == 0) {
				line(x - cameraPos.x, 0, x - cameraPos.x, areaDims.z);
			}
		}
		for (int z = 0; z < areaDims.z; z += 100) {
			line(0, z, areaDims.x, z);
		}
		popMatrix();
		popMatrix();
	}

}